/**
 *                CODE FOR MONA ROBOTS
 *  Version : 2.0
 *  1.0 version made by Guichaoua Tristant and Leveque Dorian.
 *  2.0 version made by Donjat Jérémie and Hénard Aymerick. 
 *  Warning :
 *  For debbuging, if Serial.print is used, the last print should
 *  be a Serial.println for the line return.
*/
// --------------------------------------- Includes --------------------------------------- //
#ifdef _WIN32
  #include <WinSock2.h>
  #include <windows.h>
  #include "Arduino.hpp"
  #include "WiFi.hpp"
  #include "Mona_ESP_lib.h"
  #include "Vector2.hpp"
#else
  #include <WiFi.h>
  #include <WiFiUdp.h>
  #include <Mona_ESP_lib.h>
  #include "Vector2.hpp"
#endif

// --------------------------------------- Defines --------------------------------------- //
#ifdef _WIN32
  #define SERVER "192.168.56.1"  // IP adress server
  #define PORT 10001   // Destination port
  #define BUFLEN 475  // Buffer size : Adapt it with the number of robots (For Max 40 robots : message size = 3784 bits / 473 bytes)
  #define THRESHOLD 30
#else
  #define ID_ROBOT 1
  #define BUFLEN 475
  #define THRESHOLD 120
#endif

#define BUFFERDECTECT 15
#define MAXROBOTS 40
#define MAX_SPEED 150
#define NBRULES 7
#define PI 3.1415926535897932384626433832795

// --------------------------------------- Variables initialisation --------------------------------------- //
// Socket and IP adress of Passerelle 
#ifdef _WIN32
  sockaddr_in server;
  int client_socket;
#else
  // Wifi udp instance
  WiFiUDP udp;
  const IPAddress server_ip = {192, 168, 8, 142};
  const uint16_t localPort = 10001;
#endif

// Wifi parameters :
const char *ssid = "GL-MT300N-V2-6f0";
const char *password = "goodlife";

// Structure for each neighbour :
struct neighbour{
  vector2 position;
  float angle;
};

// Nb of neighbour :
unsigned int nbNeighbours = 0;

// List of neighbour :
neighbour listNeighbours[MAXROBOTS];

// Flocking variables :
double intensity_friction = 0.1;
double intensity_attraction = 2;
double intensity_repulsion = 1;
double intensity_alignement = 1;
double intensity_randomMovement = 0.5;
double intensity_collisionAvoidance = 20;
double intensity_avoidObstacle = -20.0;

// Parameter array :
double param[NBRULES] = {intensity_friction, 
                         intensity_attraction, 
                         intensity_repulsion, 
                         intensity_alignement, 
                         intensity_randomMovement,
                         intensity_collisionAvoidance, 
                         intensity_avoidObstacle};

// Acceleration and Speed vectors :
vector2 acceleration;
vector2 speed;

// Command variable :
unsigned char kind = 0;

// Point to reach (for initialization):
vector2 point;

// Time :
double prevTime = 0;
double nextTime = 0;
double timeStep = 0;

// Buffer detection :
bool bufferDetec[BUFFERDECTECT];
int cpt = 0;
float sum = 0.0;
float moy = 0.0;

// --------------------------------------- Decode functions --------------------------------------- //
// Union of a Float 32 type
union f_u32
{
    float f;
    uint32_t i;
    char bts[4];
};
// Decode char from udp buffer
uint8_t parse_u8(char *&buf_p)
{
    uint8_t x = *buf_p;
    buf_p += sizeof(uint8_t);
    return x;
}
// Decode uint32 from udp buffer
uint32_t parse_u32_le(char *&buf_p)
{
    f_u32 conv;
    memcpy(conv.bts, buf_p, 4);
    buf_p += 4;
    return conv.i;
}
// Decode float 32 from udp buffer
float parse_f32_le(char *&buf_p)
{
    f_u32 conv;
    memcpy(conv.bts, buf_p, 4);
    buf_p += 4;
    return conv.f;
}

// --------------------------------------- Utility functions --------------------------------------- //
// Set the speed to the minimum between of current speed and the max speed :
vector2 MinSpeed(vector2 vec, double max)
{
  vector2 force;
  force.x = vec.x;
  force.z = vec.z;
  double norm = VectorSize(vec);
  if(norm > max) 
  {
    force = Normalise(vec);
    force = Mult(force, max);
  }
  return force;
}

// --------------------------------------- Robot Hardware functions --------------------------------------- //
// Led color function
void led(int r, int g, int b)
{
    Set_LED(1, r, g, b);
    Set_LED(2, r, g, b);
}

//Value between [-100, 100]
void Set_motors_intensity(float left, float right)
{
    if (left >= 0)
    {
        Left_mot_forward(left * MAX_SPEED/100.0f);
    }
    else
    {
        Left_mot_backward(-left * MAX_SPEED/100.0f);
    }
    if (right >= 0)
    {
        Right_mot_forward(right * MAX_SPEED/100.0f);
    }
    else
    {
        Right_mot_backward(-right * MAX_SPEED/100.0f);
    }
}

// Set motor speed :
void Set_speed(vector2 vec) 
{
  float right = 0;
  float left = 0;
  double norm = VectorSize(vec);
  vector2 force = Normalise(vec);

  // If we are on Webots, the Z axis is at the opposite from Optitrack
  #ifdef _WIN32
    // With a float O can be negative
    if((force.x == 0.0 || force.x == -0.0) && (force.z == 0.0 || force.z == -0.0))
    {
      left = 0; 
      right = 0;
      // Red light ON
      led(100,0,0);
    }
    else 
    {
      if(force.z <= 0)
      {
        left = 100; 
        right = 100 * force.x;
        if(norm<1.0)
        {
          left = left*norm;
          right = right*norm;
        }
        // Green light ON
        led(0,100,0);
      }
      else
      {
        right = 100;
        left = 100 * force.x;
        if(norm<1.0)
        {
          left = left*norm;
          right = right*norm;
        }
        // Green light ON
        led(0,100,0);
      } 
    }
  // If we are with Optitrack
  #else
    // With a float O can be negative
    if((force.x == 0.0 || force.x == -0.0) && (force.z == 0.0 || force.z == -0.0))
    {
      left = 0; 
      right = 0;
      // Red light ON
      led(100,0,0);
    }
    else 
    {
      if(force.z >= 0)
      {
        left = 100; 
        right = 100 * force.x;
        if(norm<1.0)
        {
          left = left*norm;
          right = right*norm;
        }
        // Green light ON
        led(0,100,0);
      }
      else
      {
        right = 100;
        left = 100 * force.x;
        if(norm<1.0)
        {
          left = left*norm;
          right = right*norm;
        }
        // Green light ON
        led(0,100,0);
      } 
    }
  #endif

  Set_motors_intensity(left,right);
}

// --------------------------------------- Flocking functions --------------------------------------- //
// This method create a random force, added to the acceleration of the agent.
// It aim to allow an agent to move randomly.
// The greater the intensity, the greater the force.
vector2 Friction(vector2 speed)
{
  vector2 force;
  force.x = 0;
  force.z = 0;
  force = Mult(speed, -1);
  force = Mult(force, param[0]);
  return force;
}

// Attraction between robots :
vector2 Attraction() 
{
  vector2 force;
  force.x = 0;
  force.z = 0;
  if(nbNeighbours>0) {
    for(unsigned int i=0; i< nbNeighbours; i++) {
      force = Add(force,listNeighbours[i].position);
    }  
    force = Divide(force, nbNeighbours);
    force = Mult(force, param[1]);
  }
  return force;
}

// Repulsion between robots :
vector2 Repulsion()
{
  vector2 force;
  force.x = 0;
  force.z = 0;
  if(nbNeighbours>0) {
    for(unsigned int i=0; i< nbNeighbours; i++) {
      vector2 temp = Mult(listNeighbours[i].position,-1);
      force = Add(force,Normalise(temp));
    } 
    force = Divide(force, nbNeighbours);
    force = Mult(force, param[2]);
  }
  return force;
}

/// Add to the current acceleration a alignment force based on current neighbours. 
/// This force align this agent to match its detected neighbours speed (direction and intensity).
vector2 Alignment()
{
  vector2 ret;
  ret.x = 0;
  ret.z = 0;
  float sum = 0;
  if(nbNeighbours>0){
    for(unsigned int i=0; i<nbNeighbours;i++)
    {
      sum += listNeighbours[i].angle;
    }
    sum = sum/nbNeighbours;
    #ifdef _WIN32
    ret.x = cos((sum/180.0*PI));
    ret.z = sin((sum/180.0*PI));
    #else
    ret.x = cos((sum/180.0*PI));
    ret.z = -sin((sum/180.0*PI));
    #endif
    ret = Mult(ret, param[3]);
    return ret;    
  }
  else{
    vector2 nullvec;
    nullvec.x = 0.0;
    nullvec.z = 0.0;
    return nullvec;
  }
}

// Move randomly :
vector2 RandomMovement()
{
  float x = ((float) random(0,100) - 50.0)/100.0;
  float z = ((float) random(0,100) - 50.0)/100.0;
  vector2 force;
  force.x = x;
  force.z = z;
  force = Normalise(force);
  force = Mult(force,param[4]);
  return force;
}

// Avoid others robots :
vector2 CollisionAvoidance()
{
  vector2 force;
  force.x = 0;
  force.z = 0;
  if(nbNeighbours>0) {
    for(unsigned int i=0; i<nbNeighbours; i++)
    {
      double dist = VectorSize(listNeighbours[i].position);
      if(dist<0.09){
        vector2 temp = Mult(listNeighbours[i].position, -1);
        temp = Normalise(temp);
        temp = Mult(temp, param[5]);
        force = Add(force, temp);
      }
    }
  }
  return force;
}

// Avoid walls and obstacles :
vector2 AvoidObstacles(int threshold)
{
  // return vector 
  vector2 ret;
  ret.x = 0.0;
  ret.z = 0.0;

  // See if the sensors detect something :
  bool c1 = Detect_object(1,threshold);
  bool c2 = Detect_object(2,threshold);
  bool c3 = Detect_object(3,threshold);
  bool c4 = Detect_object(4,threshold);
  bool c5 = Detect_object(5,threshold);

  // Obstacle angle :
  float angle = 0;
  #ifndef _WIN32
    bool obst = 0;
  #endif
  
  if(c1 || c2 || c3 || c4 || c5)
  {
    if(c1)
    {
      angle += 70;
    }
    if(c2)
    {
      angle += 35;
    }
    if(c4)
    {
      angle += -35;
    }
    if(c5)
    {
      angle += -70;
    }

    // Force direction :
    #ifdef _WIN32
      ret.x = cos(angle/(180.0*PI));
      ret.z = sin(angle/(180.0*PI));
    #else
      ret.x = cos(angle/(180.0*PI));
      ret.z = -sin(angle/(180.0*PI));

      obst = 1; 
    #endif
  }

  #ifdef _WIN32
    ret = Mult(ret, param[6]);
  #else
    // High frequency filter :
    bufferDetec[cpt] = obst;
    cpt++;
    if(cpt == BUFFERDECTECT){
      cpt = 0;
    }
    if(sizeof(bufferDetec) == BUFFERDECTECT){
      moy = 0.0;
      sum = 0.0;
      for(int i=0; i<BUFFERDECTECT; i++){
        sum += bufferDetec[i];
      }
      moy = sum/BUFFERDECTECT;
    }

    // Check if there is an obstable :
    if(moy>=0.8)
    {
      ret = Mult(ret, param[6]);
    }
    else
    {
      ret.x = 0;
      ret.z = 0;
    }
  #endif

  // Return :
  return ret;
}

// --------------------------------------- Init function -------------------------------- //
void RobotsInitialisation(vector2 point)
{
  Serial.print("X : ");
  Serial.println(point.x);
  Serial.print("Z : ");
  Serial.println(point.z);
  if(point.z > 0.01)
  {
    Set_motors_intensity(0, 100);
  }
  else if (point.z < -0.01)
  {
    Set_motors_intensity(100, 0);
  }
  else if (point.x > 0.0)
  {
    Set_motors_intensity(100, 100);
  }
  else
  {
    Set_motors_intensity(0.0, 0.0);
  }
}

// --------------------------------------- Setup --------------------------------------- //
void setup() {
  // Random Seed :
  #ifdef _WIN32
    randomSeed(ROBOT_ID);
  #else
    randomSeed(ID_ROBOT);
  #endif
  // Mona initialisation
  Mona_ESP_init();
  // Baudrate setting
  Serial.begin(115200);
  // Wifi connection
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
      delay(1000);
  }
  // Blue color mean connected to the wifi
  led(0, 0, 100);
  // Acceleration and Speed initialisation :
  acceleration.x = 0;
  acceleration.z = 0;
  speed.x = 0;
  speed.z = 0;
  // Point to reach init (for initialisation)
  point.x = 0;
  point.z = 0;
  // Timestep initialisation :
  prevTime = millis();

  // ================== If we are simulating on Webots ================== //
  #ifdef _WIN32
  // initialise winsock
  WSADATA ws;
  if (WSAStartup(MAKEWORD(2, 2), &ws) != 0)
  {
      Serial.println("Failed. Error Code: ");
      Serial.println(WSAGetLastError());
  }
  // create socket
  if ((client_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR) // <<< UDP socket
  {
      Serial.print("socket() failed with error code: ");
      Serial.println(WSAGetLastError());
  }
  else
  {
      //Serial.println("Socket created");
  }
  // Socket confifuration in non-blocking mode
  u_long nonBlocking = 1;
  if (ioctlsocket(client_socket, FIONBIO, &nonBlocking) == SOCKET_ERROR) {
      Serial.print("Configuration failed..");
  }
  // Setup address structure
  memset((char*)&server, 0, sizeof(server));
  server.sin_family = AF_INET;
  server.sin_port = htons(PORT);
  server.sin_addr.S_un.S_addr = inet_addr(SERVER);
  // Setup message
  char message[BUFLEN];
  sprintf(message, "%u", ROBOT_ID);  
  // send the message
  if (sendto(client_socket, message, strlen(message), 0, (sockaddr*)&server, sizeof(sockaddr_in)) == SOCKET_ERROR)
  {
      Serial.println("sendto() failed with error code: ");
      Serial.println(WSAGetLastError());
  }
  
  // ================== If we are with real robots ================== //
  #else
  // Open a UDP connection
  udp.begin(localPort);
  udp.beginPacket(server_ip, localPort);
  // Serial.print("UDP Client : ");
  // Serial.print(WiFi.localIP().toString().c_str());
  // Serial.print(":");
  // Serial.println(localPort);
  // Message construction
  char buf[1];
  char ID = ID_ROBOT;
  sprintf(buf, "%u", ID);
  // Send a udp packet with ID and IP adress of the robot
  udp.printf(buf);
  udp.endPacket();
  #endif
  // ==================================== //
}

// --------------------------------------- Loop --------------------------------------- //
void loop() {
  // ================== If we are simulating on Webots ================== //
  #ifdef _WIN32
    char answer[BUFLEN] = {};
    int slen = sizeof(sockaddr_in);
    int answer_length;
    if ((answer_length = recvfrom(client_socket, answer, BUFLEN, 0, (sockaddr*)&server, &slen)) == SOCKET_ERROR)
    {
      // Get the message error :
      int ierr= WSAGetLastError();
      // If the message is different from WSAEWOULDBLOCK (which means that no data are available) :
      if (ierr!=WSAEWOULDBLOCK) {  
        Serial.print("recvfrom() failed with error code: ");
        Serial.println(ierr);
      }
    }
    // If we receive a message :
    if(answer_length>0)
    {
      // Pointer pointing on the first buffer element  
      char *i = answer;
      // Get the message type
      kind = parse_u8(i);
      // Number of rules
      uint8_t nbparam = parse_u8(i);
      // For each rules update the intensity
      for(uint8_t cpt = 0; cpt < nbparam; cpt++)
      {
        param[cpt] = parse_f32_le(i);
      }
      // Depending on the message kind received 
      switch (int(kind))
      {
        case 0:{  
          break;
        }

        case 1:{
          break;
        }
        
        case 2:{
          // x point
          float x = parse_f32_le(i);
          point.x = x;
          // z point
          float z = parse_f32_le(i);
          point.z = z;
          break;
        }      
      }
      // Get the number of neighbour
      nbNeighbours = parse_u32_le(i);
      // For each neighbour
      for (uint32_t cpt = 0; cpt < nbNeighbours; ++cpt)
      {
        // Get the relative distance X
        float x = parse_f32_le(i);
        // Get the relative distance Y
        float y = parse_f32_le(i);
        // Get the relative angle
        float angle = parse_f32_le(i);
        // List nbNeighbours 
        listNeighbours[cpt].position.x = x;
        listNeighbours[cpt].position.z = y;
        listNeighbours[cpt].angle = angle;
      }
    }

  // ================== If we are with real robots ================== //
  #else
    // Packet size :
    char packetBuffer[BUFLEN];
    // Size of a udp packet
    int packetSize = udp.parsePacket();
    // If the received packet is > 0, we receive data
    if (packetSize) {
      // Read the buffer
      int len = udp.read(packetBuffer, BUFLEN);
      if (len > 0) packetBuffer[len] = 0;
      // Pointer pointing on the first buffer element  
      char *i = packetBuffer;
      // Get the message type
      kind = parse_u8(i);
      // Number of rules
      uint8_t nbparam = parse_u8(i);
      // For each rules update the intensity
      for(uint8_t cpt = 0; cpt < nbparam; cpt++)
      {
        param[cpt] = parse_f32_le(i);
      }
      // Depending on the message kind received 
      // Depending on the message kind received 
      switch (int(kind))
      {
        case 0:{  
          break;
        }

        case 1:{
          break;
        }
        
        case 2:{
          // x point
          float x = parse_f32_le(i);
          point.x = x;
          // z point
          float z = parse_f32_le(i);
          point.z = z;
          break;
        }      
      }
      // Get the number of neighbour
      nbNeighbours = parse_u32_le(i);
      // For each neighbour
      for (uint32_t cpt = 0; cpt < nbNeighbours; ++cpt)
      {
        // Get the relative distance X
        float x = parse_f32_le(i);
        // Get the relative distance Y
        float y = parse_f32_le(i);
        // Get the relative angle
        float angle = parse_f32_le(i);
        // List nbNeighbours 
        listNeighbours[cpt].position.x = x;
        listNeighbours[cpt].position.z = y;
        listNeighbours[cpt].angle = angle;
      }
    }
  #endif
  // ==================================== //

  // Next time Step (in ms):
  nextTime = millis();
  // Time step computation :
  timeStep = nextTime - prevTime;
  // TimeStep converstion in s :
  timeStep = timeStep / 1000.0;
  // Robot command :
  switch (kind)
  {
    // Stop
    case 0:{
      // Stop motors :
      Set_motors_intensity(0,0);

      // IR detection
      bool obj = false;
      for(int i=1; i<6; i++)
      {
        if(Detect_object(i, THRESHOLD))
        {
          obj = true;
        }
      }

      // High frequency filter :
      bufferDetec[cpt] = obj;
      cpt++;
      if(cpt == BUFFERDECTECT){
        cpt = 0;
      }
      if(sizeof(bufferDetec) == BUFFERDECTECT){
        moy = 0.0;
        sum = 0.0;
        for(int i=0; i<BUFFERDECTECT; i++){
          sum += bufferDetec[i];
        }
        moy = sum/BUFFERDECTECT;
      }
      
      // Set leds :
      if(moy>=0.8)
      {
        led(100,0,0);
      }
      else
      {
        led(0,100,0);
      }
      break;
    }
      
    // Flocking, swarming, etc.. :
    case 1:{
      // Acceleration reset :
      acceleration.x = 0;
      acceleration.z = 0;
      // Friction :
      acceleration = Friction(speed);
      // Attraction, Repulsion, Alignement, CollisionAvoidance, RandomMovment, AvoidObstacles :
      acceleration = Add(acceleration, Attraction());
      acceleration = Add(acceleration, Repulsion()); 
      acceleration = Add(acceleration, Alignment());
      acceleration = Add(acceleration, CollisionAvoidance());
      acceleration = Add(acceleration, RandomMovement());
      acceleration = Add(acceleration, AvoidObstacles(THRESHOLD));
      // Update Speed
      speed = Add(speed,Mult(acceleration,(float)(timeStep)));
      speed = MinSpeed(speed, 1.0);
      Set_speed(speed);
      break;
    }
      
    // Initialisation
    case 2:{
      RobotsInitialisation(point);
      break;
    } 
  }

  // ================== If we are simulating on Webots ================== //
  #ifdef _WIN32
    // Setup feedback
    char feedback[BUFLEN];
    int j;
    j =  sprintf(feedback, "%u", ROBOT_ID);
    j += sprintf(feedback+j, "%s", "_");
    j += sprintf(feedback+j, "%u", kind);
    j += sprintf(feedback+j, "%s", "_");
    j += sprintf(feedback+j, "%u", nbNeighbours);
    j += sprintf(feedback+j, "%s", "_");
    j += sprintf(feedback+j, "%.2f", speed.x);
    j += sprintf(feedback+j, "%s", "_");
    j += sprintf(feedback+j, "%.2f", speed.z);
    
    // send the message
    if (sendto(client_socket, feedback, strlen(feedback), 0, (sockaddr*)&server, sizeof(sockaddr_in)) == SOCKET_ERROR)
    {
        Serial.println("sendto() failed with error code: ");
        Serial.println(WSAGetLastError());
    }

  // ================== If we are with real robots ================== //
  #else
    udp.beginPacket(server_ip, localPort);

    // Setup feedback
    char feedback[BUFLEN];
    int j;
    char ID = ID_ROBOT;
    j =  sprintf(feedback, "%u", ID_ROBOT);
    j += sprintf(feedback+j, "%s", "_");
    j += sprintf(feedback+j, "%u", kind);
    j += sprintf(feedback+j, "%s", "_");
    j += sprintf(feedback+j, "%u", nbNeighbours);
    j += sprintf(feedback+j, "%s", "_");
    j += sprintf(feedback+j, "%.2f", speed.x);
    j += sprintf(feedback+j, "%s", "_");
    j += sprintf(feedback+j, "%.2f", speed.z);

    // send the message
    udp.printf(feedback);
    udp.endPacket();
  #endif
  // ==================================== //

  // Time save :
  prevTime = millis();
  
  // Little delay (same than the Webots basicTimeStep):
  #ifndef _WIN32
    delay(32);
  #endif
}