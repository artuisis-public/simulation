"""supervisor_controller controller."""
# Import
import socket
import struct
from dataclasses import dataclass
from random import random
from typing import Any, Tuple
# Webot imports :
from controller import Supervisor

# IP Adress and Port :
IP_adress = "192.168.56.2"
destPort = 20001
selfPort = 20002

# World parameters (Spawn zone)
FLOOR_SIZE = (3, 3)
Z = 0.0204

# Number of robots (MAX = 40):
ROBOT_COUNT = 8

# Supervisor creation :
supervisor = Supervisor()

# Byte message options :
u32_t = struct.Struct("<L")
vec2_t = struct.Struct("<ff")
f32_t = struct.Struct("<f")

# Robot Handler :
@dataclass
class RobotHandler:
    # Robot parameters 
    id: int
    translation_field: Any
    rotation_field: Any
    controller_field: Any

    # Get the robot position :
    def get_position(self) -> tuple[float, float, float]:
        [x, y, z] = self.translation_field.getSFVec3f()
        return x, y, z

    # Get the robot rotation :
    def get_rotation(self) -> float:
        [ax, ay, az, angle] = self.rotation_field.getSFRotation()

        # on WeBot Z is the vertical axis
        return ax, ay, az, angle


# Robots initialisation :
def init():
    # Get node where robots will be created :
    robots_node = supervisor.getFromDef('robots').getField('children')
    # Create positions :
    positions = list[Tuple[float, float]]()

    # Robot creation :
    for i in range(1,ROBOT_COUNT+1):
        # Import the robot and set its controller and ID
        parameters = 'Mona{ controller "mona" controllerArgs "'+str(i)+'"}'
        robots_node.importMFNodeFromString(0, parameters)
        # Get the imported robot
        rb = robots_node.getMFNode(0)
        # Get translation field
        translation = rb.getField('translation')
        # Get rotation field
        rotation = rb.getField('rotation')
        # Get controller field
        controller = rb.getField('controller')

        # Randomly place the robots on the scene :
        while True:
            # X and Y creation
            x = (random() - 0.5) * FLOOR_SIZE[0] * 0.95
            y = (random() - 0.5) * FLOOR_SIZE[1] * 0.95
            # Check if two robots are in collision :
            for pos in positions:
                dx = x - pos[0]
                dy = y - pos[1]
                dist = dx*dx + dy*dy
                if dist < 0.1:
                    break
            # If not :
            else:
                positions.append((x, y))
                translation.setSFVec3f([x, y, Z])
                break

        # Put the new created robot in the handlers :
        handlers.append(RobotHandler(i, translation, rotation, controller))
    
    # Return the handlers :
    return handlers

# Running function :
def run(handlers):
    # Socket creation :
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((IP_adress, selfPort))
    # Time of the simulated world :
    timestep = int(supervisor.getBasicTimeStep())
    # While the simulation is still running :
    while supervisor.step(timestep) != -1:
        # Message constuction :
        msg = ''
        # Nb of robots :
        #msg += str(len(handlers))+'_'
        # For each robot :
        for h in handlers:
            msg += str(h.id)+'=' # Add the ID
            msg += str(h.get_position())+'_' # Add the robot position
            msg += str(h.get_rotation())+';' # Add the robot rotation
            # msg = bytearray(struct.pack("I", h.id)) + \
            # bytearray(struct.pack("f", h.get_position()[0])) + \
            # bytearray(struct.pack("f", h.get_rotation()[3]))
        msg = msg[:-1]
        message = bytes(msg, 'utf-8')
        # Send robots position to the Passerelle :
        sock.sendto(message, (IP_adress, destPort))
        #sock.sendto(msg, (IP_adress, destPort))
        #time.sleep(0.1)
    

# Main :
if __name__ == '__main__':
    # Create robot handler :
    handlers = list[RobotHandler]()
    hdls = init()
    run(hdls)