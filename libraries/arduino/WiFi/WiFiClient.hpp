/*
  WiFiClient.cpp - Library for Arduino WiFi shield.
  Copyright (c) 2011-2014 Arduino LLC.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef wificlient_h
#define wificlient_h
#include "IPAddress.hpp"
#include "WiFi.hpp"

class WiFiClient {

public:
  WiFiClient()
  : WiFiClient(MAX_SOCK_NUM)
  {}
  
  WiFiClient(uint8_t sock)
  : _sock(sock)
  {}

  ~WiFiClient() {}

  uint8_t status();
  int connect(IPAddress ip, uint16_t port);
  int connect(const String host, uint16_t port);
  size_t write(uint8_t b);
  size_t write(const uint8_t *buf, size_t size);
  int available();
  int read();
  int read(uint8_t *buf, size_t size);
  int peek();
  void flush();
  void stop();
  uint8_t connected();
  operator bool();
  int print(const String buf); // function herited from Client of Arduino
  int println(const String buf); // function herited from Client of Arduino
  String readStringUntil(char buf);

  friend class WiFiServer;

private:
  static uint16_t _srcport;
  int8_t _sock;   //not used
  int16_t  _socket;
  bool _connected = false;
};

#endif
