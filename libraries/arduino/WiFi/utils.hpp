#ifndef utils_hpp
#define utils_hpp

#include <vector>
#include <string>

#if defined _WIN32
# define SYSCALL_ERRNO (GetLastError())
# define RESTART_SYSCALL(result,syscall) \
         do { (result)=(syscall); } while(0) // no restart actually!
#else
# define SYSCALL_ERRNO (errno)
# define RESTART_SYSCALL(result,syscall) \
         do { (result)=(syscall); } while(((result)<0)&&(errno==EINTR))
#endif

#endif