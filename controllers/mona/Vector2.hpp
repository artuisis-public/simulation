#include <math.h>

struct vector2{
  float x;
  float z;  
};

double VectorSize(vector2 vec) 
{
  return (double)sqrt(vec.x*vec.x+vec.z*vec.z);
} 

vector2 Normalise(vector2 vec) 
{
  vector2 res;
  double dist = VectorSize(vec);
  if(dist != 0){
    res.x = vec.x/dist;
    res.z = vec.z/dist; 
  }
  else{
    res.x = 0;
    res.z = 0;
  }
  return res;
}

vector2 Add(vector2 vec1, vector2 vec2) 
{
  vector2 res;
  res.x = vec1.x + vec2.x;
  res.z = vec1.z + vec2.z;
  return res;
}

vector2 Mult(vector2 vec, float value) 
{
  vector2 res;
  res.x = vec.x * value;
  res.z = vec.z * value;
  return res;
}

vector2 Divide(vector2 vec, float value) 
{
  vector2 res;
  res.x = vec.x / value;
  res.z = vec.z / value;
  return res;
}