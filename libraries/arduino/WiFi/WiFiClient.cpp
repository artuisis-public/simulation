/*
  WiFiClient.cpp - Library for Arduino WiFi shield.
  Copyright (c) 2011-2014 Arduino LLC.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

extern "C" {
  #include "utility/wl_definitions.h"
  #include "utility/wl_types.h"
}

#include "WiFiClient.hpp"
#include "WiFiServer.hpp"
#include "utility/server_drv.h"
#include "iostream"


uint16_t WiFiClient::_srcport = 1024;

int WiFiClient::connect(const String host, uint16_t port) {
	IPAddress remote_addr;
	if (WiFi.hostByName(host.c_str(), remote_addr))
	{
		return connect(remote_addr, port);
	}
	return 0;
}

int WiFiClient::connect(IPAddress ip, uint16_t port) {

  #ifdef _WIN32
  struct sockaddr_in address;
  struct hostent *server;
    /* initialize the socket api */
    WSADATA info;

    _socket = WSAStartup(MAKEWORD(1, 1), &info); /* Winsock 1.1 */
    if (_socket != 0) {
      printf("cannot initialize Winsock\n");

      return -1;
    }

  _socket = socket(AF_INET, SOCK_STREAM, 0);
  if (_socket == -1) {
    printf("cannot create socket\n");
    return -1;
  }
 
  
  /* fill in the socket address */
  /*memset(&address, 0, sizeof(struct sockaddr_in));
  address.sin_family = AF_INET;
  address.sin_port = htons(port);*/

  uint32_t ip_addr = ip;
  struct sockaddr_in serveraddr;
  memset((char *) &serveraddr, 0, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  memcpy((void *) &serveraddr.sin_addr.s_addr, (const void *)(&ip_addr), 4);
  serveraddr.sin_port = htons(port);

  /*if (server)
    memcpy((char *)&address.sin_addr.s_addr, (char *)server->h_addr, server->h_length);
  else {
    printf("cannot resolve server name: %s\n", ip.toString().c_str());
    stop();
    return 0;
  }*/

  /* connect to the server */
  int rc = ::connect(_socket, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
  if (rc == -1) {
    printf("cannot connect to the server\n");
    stop();
    return 0;
  }
  u_long mode = 1;
  ioctlsocket(_socket,FIONBIO,&mode);
    
  #else

  _socket = socket(AF_INET, SOCK_STREAM, 0);
  if (_socket < 0) {
    printf("socket %d", errno);
    return 0;
  }

  fcntl(_socket, F_SETFL, fcntl(_socket, F_GETFL, 0) | O_NONBLOCK);
  uint32_t ip_addr = ip;
  struct sockaddr_in serveraddr;
  memset((char *) &serveraddr, 0, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  memcpy((void *) &serveraddr.sin_addr.s_addr, (const void *)(&ip_addr), 4);
  serveraddr.sin_port = htons(port);
  fd_set fdset;
  FD_ZERO(&fdset);
  FD_SET(_socket, &fdset);

  int res = ::connect(_socket, (struct sockaddr*)&serveraddr, sizeof(serveraddr));
  if (res < 0 && errno != EINPROGRESS) {
    printf("connect on fd %d, errno: %d, \"%s\"", _socket, errno, strerror(errno));
    stop();
    return 0;
  }

  res = select(_socket + 1, nullptr, &fdset, nullptr, nullptr);
  if (res < 0) {
      printf("select on fd %d, errno: %d, \"%s\"", _socket, errno, strerror(errno));
      stop();
      return 0;
  } else if (res == 0) {
      printf("select returned due to timeout %d ms for fd %d", -1, _socket);
      stop();
      return 0;
  } else {
      int sockerr;
      socklen_t len = (socklen_t)sizeof(int);
      res = getsockopt(_socket, SOL_SOCKET, SO_ERROR, &sockerr, &len);

      if (res < 0) {
          printf("getsockopt on fd %d, errno: %d, \"%s\"", _socket, errno, strerror(errno));
          stop();
          return 0;
      }

      if (sockerr != 0) {
          printf("socket error on fd %d, errno: %d, \"%s\"", _socket, sockerr, strerror(sockerr));
          stop();
          return 0;
      }
  }
  fcntl( _socket, F_SETFL, fcntl( _socket, F_GETFL, 0 ) & (~O_NONBLOCK) );

  #endif
  _connected = true;
  return 1;
}

size_t WiFiClient::write(uint8_t b) {
  #ifndef _WIN32
  return write(&b, 1);
  #else
  return 0;
  #endif
}

size_t WiFiClient::write(const uint8_t *buf, size_t size) {
  #ifndef _WIN32
  return ::write(_socket, buf, size);
  #else
  return 0;
  #endif
}

int WiFiClient::available() {
  if (_socket != -1)
  {
      int value;
      #ifdef _WIN32
      char buf;
      unsigned long l;
      ioctlsocket(_socket, FIONREAD, &l);
      value = (int) l;
      #else
      ioctl(_socket, FIONREAD, &value);
      #endif
      return value;
  }
  return 0;
}

int WiFiClient::read() {
  char buf;
  #ifdef _WIN32
  return (::recv(_socket, &buf, 1, 0)) ? buf: -1;
  #else
  return (::read(_socket, &buf, 1)) ? buf: -1;
  #endif
}

int WiFiClient::read(uint8_t* buf, size_t size) {
  #ifdef _WIN32
  char* b = reinterpret_cast<char*>(buf);
  return ::recv(_socket, b, size, 0);
  buf = reinterpret_cast<uint8_t*>(b);
  #else
  return ::read(_socket, buf, size);
  #endif
}

int WiFiClient::peek() {
	  /*uint8_t b;
	  if (!available())
	    return -1;

	  ServerDrv::getData(_sock, &b, 1);*/
	  return 0;
}

void WiFiClient::flush() {
  // TODO: a real check to ensure transmission has been completed
}

void WiFiClient::stop() {
  #ifdef _WIN32
  closesocket(_socket);
#else
  close(_socket);
  _connected = false;
#endif
}

uint8_t WiFiClient::connected() {
  uint8_t s = status();
  //printf("status %d", s);
  return !(s == LISTEN || s == CLOSED || s == FIN_WAIT_1 ||
      s == FIN_WAIT_2 || s == TIME_WAIT ||
      s == SYN_SENT || s== SYN_RCVD ||
      (s == CLOSE_WAIT));
}

uint8_t WiFiClient::status() {
  if (_connected) {
    if (available() < 0) return CLOSED;
    else return ESTABLISHED;
  }
  return CLOSED;
}

WiFiClient::operator bool() {
  return _socket != -1;
}

int WiFiClient::print(const String buf) // function herited from Client of Arduino
{
  return ::send(_socket, buf.c_str(), buf.length(), 0);
}

int WiFiClient::println(const String buf) // function herited from Client of Arduino
{
  String buffer = buf;
  //buffer += '\n'; // apparently it's not added in arduino
  return ::send(_socket, buffer.c_str(), buffer.length(), 0);
}

String WiFiClient::readStringUntil(char value) 
{
  String result;
  uint8_t c;
  while(read(&c, 1)==1)
  {
    result+=c;
    if(c==value)
    {
      break; // end of line
    }
  }
  return result;
}
